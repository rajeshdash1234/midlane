package com.midlane.midlanefinance;

public interface ConnectionLostCallback {
    public void connectionLost();
}
