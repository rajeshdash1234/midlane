package com.midlane.midlanefinance;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.matm.matmsdk.Bluetooth.BluetoothActivity;
import com.matm.matmsdk.MPOS.BluetoothServiceActivity;
import com.matm.matmsdk.MPOS.PosServiceActivity;
import com.matm.matmsdk.Utils.EnvData;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.AEPS2HomeActivity;
import com.matm.matmsdk.aepsmodule.BioAuthActivity;
import com.midlane.midlanefinance.login.LoginActivity;
import com.midlane.midlanefinance.utility.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private EditText amnt_txt, loan_id;
    private Button aeps_submit_btn,matm_submit_btn,btnPair,btnlogout;
    RadioGroup rg_trans_type;
    RadioButton rb_cw, rb_be;
    String strTransType = "";
    Boolean respflag = false;
    private static final int  REQUEST_ACCESS_FINE_LOCATION = 111;
    String token="",userName="";
    ProgressDialog dialog;
    String checkSdkFlag = "";
    SessionManager sessionManager;
    Boolean bio_auth_status = false;


    Double latitude, longitude;
    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    String postalCode = "751017";
    private static final int REQUEST_CA_PERMISSIONS = 931;
    private static final int REQUEST_WRITE_PERMISSION = 1001;
    int pincode = 751017;

    UsbManager musbManager;
    String manufactureFlag = "";
    boolean aeps_transaction_intent = false;
    private UsbDevice usbDevice;
    private Boolean location_flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setUpGClient();
        musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        initView();
    }

    private void initView() {

        sessionManager = new SessionManager(getApplicationContext());

        amnt_txt = findViewById(R.id.amnt_txt);
        loan_id = findViewById(R.id.loan_id);
        aeps_submit_btn = findViewById(R.id.aeps_submit_btn);
        matm_submit_btn = findViewById(R.id.matm_submit_btn);
        rb_cw = findViewById(R.id.rb_cw);
        rb_be = findViewById(R.id.rb_be);
        rg_trans_type = findViewById(R.id.rg_trans_type);
        btnPair = findViewById(R.id.btnPair);
        btnlogout = findViewById(R.id.btnlogout);
        dialog = new ProgressDialog(MainActivity.this);
        token  = getIntent().getStringExtra("token");
        userName = getIntent().getStringExtra("username");

        rb_cw.setChecked(true);
        strTransType = "Cash Withdrawal";

/*        HashMap<String, String> user = sessionManager.getUserDetails();
        token = user.get(SessionManager.KEY_TOKEN);
        String adminName = user.get(SessionManager.KEY_ADMIN);
        userName = user.get(SessionManager.userName);*/


        System.out.println("Token:"+token);
        //System.out.println("AdminName:"+adminName);
        System.out.println("UserName:"+userName);
        //EnvData.UserName = userName;


        boolean hasPermissionLocation = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermissionLocation) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_FINE_LOCATION);
        }


        rg_trans_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_cw:
                        strTransType = "Cash Withdrawal";
                        amnt_txt.setText("");
                        amnt_txt.setEnabled(true);
                        amnt_txt.setVisibility(View.VISIBLE);
                        amnt_txt.setInputType(InputType.TYPE_CLASS_NUMBER);

                        break;
                    case R.id.rb_be:
                        strTransType = "Balance Enquiry";
                        amnt_txt.setText("");
                        amnt_txt.setVisibility(View.GONE);
                        amnt_txt.setClickable(false);
                        amnt_txt.setEnabled(false);
                        break;
                }
            }
        });
        matm_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                matm_submit_btn.setEnabled(false);
                refreshToken("MATM2");
            }
        });
        aeps_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //refreshToken("AEPS2");

                if (biometricDeviceConnect()) {
                    // showLoader();
                    aeps_submit_btn.setEnabled(false);
                    refreshToken("AEPS2");
                } else {
                    aeps_submit_btn.setEnabled(true);
                    Toast.makeText(MainActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();

                }
            }
        });
        btnPair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnPair.setEnabled(false);
                refreshToken("PAIR");
            }
        });
        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnlogout.setEnabled(false);
                LogoutFunction();
            }
        });

        SdkConstants.SHOP_NAME = "Midland";
        SdkConstants.BRAND_NAME = "Midland";
        SdkConstants.transactionType = SdkConstants.cashWithdrawal;

    }



    private void LogoutFunction() {

        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Do you want to logout?";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        SdkConstants.LogOut="0"; //For logout from SDK end
                        SdkConstants.BlueToothPairFlag="0";
                        sessionManager.removeLoggedIn();

                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void callBtnPair() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
            } else {
                SdkConstants.applicationType = "CORE";
                Intent intent = new Intent(MainActivity.this, BluetoothServiceActivity.class);
                intent.putExtra("userName", EnvData.UserName);
                intent.putExtra("user_id", EnvData.user_id);
                intent.putExtra("user_token", token);
                startActivity(intent);
            }
        } else {


        }
    }

    private void callAeps2() {
        hideLoader();
        SdkConstants.transactionAmount = amnt_txt.getText().toString().trim();
        if (rb_cw.isChecked()){
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
            SdkConstants.transactionAmount = amnt_txt.getText().toString();


            if(amnt_txt.getText().toString().trim().length()== 0){
                Toast.makeText(MainActivity.this,"Please enter minimum Rs 1 ",Toast.LENGTH_SHORT).show();
                onResume();
            }else if(Integer.parseInt(amnt_txt.getText().toString())<1){
                Toast.makeText(MainActivity.this,"Please enter minimum Rs 1 ",Toast.LENGTH_SHORT).show();
                onResume();
            }
            else if(loan_id.getText().toString().trim().length()==0){
                Toast.makeText(MainActivity.this,"Please enter loan ID",Toast.LENGTH_SHORT).show();
                onResume();
            }
            else{
                SdkConstants.applicationType = "CORE";
                SdkConstants.tokenFromCoreApp = token;
                //SdkConstants.userNameFromCoreApp = _admin;
                SdkConstants.applicationUserName = userName;
                SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
                SdkConstants.DRIVER_ACTIVITY = "com.midlane.midlanefinance.DriverActivity";
                SdkConstants.bioauth = bio_auth_status;
                SdkConstants.paramA = "";
                SdkConstants.paramB = "";
                SdkConstants.paramC = loan_id.getText().toString();

                Intent intent = new Intent(MainActivity.this, BioAuthActivity.class);
                intent.putExtra("manufaturName","");
                startActivityForResult(intent, SdkConstants.REQUEST_CODE);
            }

        }

        if(rb_be.isChecked()){
             if(loan_id.getText().toString().trim().length()==0){
                Toast.makeText(MainActivity.this,"Please enter loan ID",Toast.LENGTH_SHORT).show();
            }else {
                 SdkConstants.transactionType = SdkConstants.balanceEnquiry;
                 SdkConstants.transactionAmount = "1";

                 SdkConstants.applicationType = "CORE";
                 SdkConstants.tokenFromCoreApp = token;
                 SdkConstants.userNameFromCoreApp = userName;
                 SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
                 SdkConstants.DRIVER_ACTIVITY = "com.midlane.midlanefinance.DriverActivity";
                 SdkConstants.bioauth = bio_auth_status;

                 SdkConstants.paramA = "";
                 SdkConstants.paramB = "";
                 SdkConstants.paramC = loan_id.getText().toString();

                 Intent intent = new Intent(MainActivity.this, BioAuthActivity.class);
                 intent.putExtra("manufaturName", "");
                 startActivityForResult(intent, SdkConstants.REQUEST_CODE);
             }

        }
    }

    private void callMATM2() {
        try {
            hideLoader();
            if (strTransType.equalsIgnoreCase("Cash Withdrawal")) {
                SdkConstants.transactionType = SdkConstants.cashWithdrawal;
                SdkConstants.transactionAmount = amnt_txt.getText().toString();

                if(amnt_txt.getText().toString().trim().length()== 0){
                    Toast.makeText(MainActivity.this,"Please enter minimum Rs 100 ",Toast.LENGTH_SHORT).show();
                    onResume();
                }else if(Integer.parseInt(amnt_txt.getText().toString())<100){
                    Toast.makeText(MainActivity.this,"Please enter minimum Rs 100 ",Toast.LENGTH_SHORT).show();
                    onResume();
                }
                else if(loan_id.getText().toString().trim().length()==0){
                    Toast.makeText(MainActivity.this,"Please enter loan ID",Toast.LENGTH_SHORT).show();
                    onResume();
                } else {

                    SdkConstants.applicationType = "CORE";
                    SdkConstants.tokenFromCoreApp = token;
                    SdkConstants.userNameFromCoreApp = EnvData.UserName;
                    SdkConstants.paramA = "";
                    SdkConstants.paramB = "";
                    SdkConstants.paramC = loan_id.getText().toString();
                    SdkConstants.loginID = "";
                    Intent intent = new Intent(MainActivity.this, PosServiceActivity.class);
                    startActivity(intent);
                }
               // finish();

                //for balance enquiry ------

            }else if (strTransType.equalsIgnoreCase("Balance Enquiry")) {

                SdkConstants.transactionType = SdkConstants.balanceEnquiry;
                SdkConstants.transactionAmount = "0";

                SdkConstants.applicationType = "CORE";
                SdkConstants.tokenFromCoreApp = token;
                SdkConstants.userNameFromCoreApp = EnvData.UserName;
                SdkConstants.paramA = "";
                SdkConstants.paramB = "";
                SdkConstants.paramC = loan_id.getText().toString();
                SdkConstants.loginID = "";
                Intent intent = new Intent(MainActivity.this, PosServiceActivity.class);
                startActivity(intent);
//                finish();
            }else{
                Toast.makeText(this, "Choose transaction type.", Toast.LENGTH_SHORT).show();
                matm_submit_btn.setEnabled(true);
            }
        }
        catch (Exception e) {

        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        //private Button aeps_submit_btn,matm_submit_btn,btnPair,btnlogout;

        aeps_submit_btn.setEnabled(true);
        matm_submit_btn.setEnabled(true);
        btnPair.setEnabled(true);
        btnlogout.setEnabled(true);

        if(respflag){
            String str = SdkConstants.responseData;
            respflag =false;

        }else{
            System.out.println("No Data");
        }

    }
    private void refreshToken(String type){
        showLoader();
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v87")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            hideLoader();
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            refreshTokenEncodedURL(token,encodedUrl,type);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        onResume();
                        Toast.makeText(MainActivity.this, "Please try again..check issue regarding token ", Toast.LENGTH_LONG).show();
                    }
                });
    }
    private void refreshTokenEncodedURL(String tokenStr, String encodedUrl,String type) {
        try {
            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject respObj =  new JSONObject(response.toString());
                                token = respObj.getString("token");
                                hideLoader();
                                if(type.equalsIgnoreCase("AEPS2")){
                                    callAeps2();
                                }
                                else if(type.equalsIgnoreCase("MATM2")){
                                    callMATM2();
                                }
                                else if(type.equalsIgnoreCase("PAIR")){
                                    callBtnPair();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            System.out.println("ERROR: "+anError.getErrorDetail());
                            hideLoader();
                            clearSession();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            hideLoader();
        }
    }

    public void showLoader() {
        try {
            if (!isFinishing() && !isDestroyed()) {
                dialog = new ProgressDialog(MainActivity.this);
                dialog.setMessage("please wait..");
                dialog.setCancelable(false);
                dialog.show();
            }
        }catch (Exception e){

        }
    }

    public void hideLoader() {
        try {
            if(!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }catch (Exception e){

        }
    }

    private void clearSession() {

        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session Expired!!! Logout";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        SdkConstants.LogOut="0"; //For logout from SDK end
                        SdkConstants.BlueToothPairFlag="0";
                        sessionManager.removeLoggedIn();

                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }


/*    private void checkAddressStatus(String _token) {
        showLoader();
        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v81")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            checkAddressStatus1(encodedUrl, _token);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(MainActivity.this, "Service is unavailable for this user, please contact our help desk for details.", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void checkAddressStatus1(String url, String _token) {
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if (obj.has("response")) {
                                JSONObject res_obj = obj.getJSONObject("response");
                                String pin = res_obj.getString("pincode");
                                String state = res_obj.getString("state");
                                String city = res_obj.getString("city");
                                bio_auth_status = res_obj.getBoolean("bioauth");
                                if (!bio_auth_status) {
                                    JSONObject jsonObject = new JSONObject();

                                    if (postalCode.length() != 0) {
                                        pincode = Integer.valueOf(postalCode);
                                    }
                                    jsonObject.put("pin", pincode);
                                    getAddressFromPin(jsonObject, _token);
                                } else {
                                    hideLoader();
                                    sendAEPS2Intent(bio_auth_status, _token);
                                }
                            } else {

                                JSONObject jsonObject = new JSONObject();
                                if (postalCode.length() != 0) {
                                    pincode = Integer.valueOf(postalCode);
                                }
                                jsonObject.put("pin", pincode);
                                getAddressFromPin(jsonObject, _token);
                                //showPinCodeDialog();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                            Toast.makeText(MainActivity.this, "Something went wrong, please contact our help desk for details.", Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(MainActivity.this, "Service is unavailable for this user, please contact our help desk for details.", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getAddressFromPin(JSONObject obj, String _token) {
        // showLoader();
        AndroidNetworking.post("https://us-central1-iserveustaging.cloudfunctions.net/pincodeFetch/api/v1/getCitystateAndroid")
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());
                            JSONObject data_obj = obj.getJSONObject("data");
                            String status = data_obj.getString("status");
                            if (status.equalsIgnoreCase("success")) {
                                JSONObject data_pin = data_obj.getJSONObject("data");
                                String pincode = data_pin.getString("pincode");
                                String state = data_pin.getString("state");
                                String city = data_pin.getString("city");
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("state", state);
                                jsonObject.put("pincode", pincode);
                                jsonObject.put("city", city);
                                String lat = "0.0";
                                String lng = "0.0";

                                if (mylocation != null) {
                                    lat = String.valueOf(mylocation.getLatitude());
                                    lng = String.valueOf(mylocation.getLongitude());
                                }
                                jsonObject.put("latLong", latitude + "," + longitude);
                                setAddress(jsonObject, _token);
                            } else {
                                hideLoader();
                                Toast.makeText(MainActivity.this, "Invalid area pin, please try after sometimes", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(MainActivity.this, "Invalid area pin, please try again.", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void setAddress(JSONObject objj, String _token) {

        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v82")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            setAddress1(objj, encodedUrl, _token);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }


    private void setAddress1(JSONObject obj, String url, String _token) {

        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());

                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if (status.equalsIgnoreCase("0")) {
                                hideLoader();
                                sendAEPS2Intent(bio_auth_status, _token);
                            } else {
                                Toast.makeText(MainActivity.this, statusDesc, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();


    }*/

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (location_flag == false) {
            if (mylocation != null) {
                location_flag = true;
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                Log.e("latitude", "latitude--" + latitude);
                try {
                    Log.e("latitude", "inside latitude--" + latitude);
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);

                    if (addresses != null && addresses.size() > 0) {
                        String address = addresses.get(0).getAddressLine(0);
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        postalCode = addresses.get(0).getPostalCode();
                        if (postalCode == null) {
                            postalCode = "751017";
                        }

                        String knownName = addresses.get(0).getFeatureName();
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkPermission() {
        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

            final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {
                // ActivityCompat.requestPermissions(getActivity(), permissionsToRequest.toArray(new String[permissionsToRequest.size()]), REQUEST_CA_PERMISSIONS);
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CA_PERMISSIONS);

            } else {
                getMyLocation();
                //retriveAUTH();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        int permissionLocation = ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        } else {
            Toast.makeText(this, "Please accept the location permission", Toast.LENGTH_SHORT).show();

            if (!isFinishing()) {
                finish();
            }
        }

        if (grantResults.length > 0 && permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // check whether storage permission granted or not.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do what you want;
                /*if (bluetoothConnector) {
                  //  showLoader();
                    refreshToken(strAepsType);
                } else {*/
                    if (biometricDeviceConnect()) {
                       // showLoader();
                        refreshToken("AEPS2");
                    } else {
                        Toast.makeText(MainActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();

                    }


               // }
            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                            .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(MainActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(MainActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    private Boolean biometricDeviceConnect() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            deviceConnectMessgae();
            return false;
        } else {
            for (UsbDevice device : connectedDevices.values()) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.getManufacturerName() != null) {
                        usbDevice = device;
                        manufactureFlag = usbDevice.getManufacturerName();
                        return true;
                    }

                }
            }
        }
        return false;
    }

    private void deviceConnectMessgae() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MainActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle("No device found")
                .setMessage("Unable to find biometric device connected . Please connect your biometric device for AEPS transactions.")
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // finish();
                    }
                })
                .show();
    }

    public void showAlertMessage(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("");
            builder.setMessage(msg);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}