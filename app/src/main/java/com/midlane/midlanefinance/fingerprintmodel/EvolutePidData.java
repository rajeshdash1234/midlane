package com.midlane.midlanefinance.fingerprintmodel;



import com.midlane.midlanefinance.fingerprintmodel.uid.Data;
import com.midlane.midlanefinance.fingerprintmodel.uid.Skey;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "EvolutePidData")
public class EvolutePidData {

    public EvolutePidData() {
    }
    @Element(name = "Resp",required = false)
                public Resp _Resp;

    @Element(name = "DeviceInfo",required = false)
    public EvoluteDeviceInfo _DeviceInfo;

    @Element(name = "Skey", required = false)
    public Skey _Skey;

     @Element(name = "Hmac",required = false)
    public String Hmac;

     @Element(name = "Data", required = false)
    public Data _Data;

}
