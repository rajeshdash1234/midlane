package com.midlane.midlanefinance.login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.matm.matmsdk.Utils.EnvData;
import com.midlane.midlanefinance.MainActivity;
import com.midlane.midlanefinance.R;
import com.midlane.midlanefinance.utility.ConnectionDetector;
import com.midlane.midlanefinance.utility.SessionManager;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    TextInputEditText user_name, password;
    Button submit;
    SessionManager session;
    String _user_name, _password;
    ProgressBar progressBar;
    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user_name = findViewById(R.id.user_name);
        password = findViewById(R.id.password);
        submit = findViewById(R.id.submit);
        progressBar = findViewById(R.id.progressBar);

        loginPresenter = new LoginPresenter(this);


        // Session Manager
        session = new SessionManager(getApplicationContext());


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionDetector cd = new ConnectionDetector(LoginActivity.this);
                if (cd.isConnectingToInternet()) {
                    _user_name = user_name.getText().toString().trim();
                    _password = password.getText().toString().trim();

                    if (_user_name.length() != 0 && _password.length() != 0) {
                        progressBar.setVisibility(View.VISIBLE);
                        loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");
                    } else {
                        Toast.makeText(LoginActivity.this, "Please check login credential", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(LoginActivity.this,"No internet connection", Toast.LENGTH_LONG).show();
                }

            }
        });
    }




    private void loadLogin(String base64) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("username", _user_name);
            obj.put("password", _password);
            AndroidNetworking.post(base64)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String _token = obj.getString("token");
                                String _admin = obj.getString("adminName");

                                // Use user real data
                                session.createLoginSession(_token, _admin);

                                getUserId(_token, "https://mobile.9fin.co.in/user/user_details");



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(LoginActivity.this, "Incorrect login and password", Toast.LENGTH_LONG).show();

                        }
                    });
        }catch ( Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void fetchedV1Response(boolean status, String response) {
        if (response != null) {
            loadLogin(response);
        }
    }



    private String getDeviceID(){
        return Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }



    public void showSessionAlert(String message){
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
       // String message = "Session is already running !!! Please login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        //finish();
                    }
                })
                .show();
    }

    public void getUserId(String token, String urlString) {
        AndroidNetworking.get(urlString)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String id = obj.getString("id");
                            String user_name = obj.getString("username");
                            EnvData.user_id = id;
                            EnvData.UserName = user_name;

                            session.createLoginSession_Username(user_name);
                            progressBar.setVisibility(View.INVISIBLE);

                            HashMap<String, String> user = session.getUserDetails();
                            String token = user.get(SessionManager.KEY_TOKEN);

                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            i.putExtra("token",token);
                            i.putExtra("username",EnvData.UserName);
                            startActivity(i);
                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        System.out.println("Error  " + anError.getErrorDetail());
                    }
                });
    }
}
